import React from 'react';
import Technology from "./components/Technology/Technology";
import "./Technologies.scss";
import "./components/Technology/Technology.scss";

const technologies = [
    {
        title: "Proficient",
        list: [
            {
                name: "SCSS",
                image: require("./img/sass.png"),
            },
            {
                name: "React",
                image: require("./img/react.png"),
            },
            {
                name: "Redux",
            },
            {
                name: "Javascript",
                image: require("./img/javascript.png"),
            },
            {
                name: "CSS",
                image: require("./img/css.png"),
            },
            {
                name: "HTML",
                image: require("./img/html.png"),
            },
            {
                name: "Node",
                image: require("./img/node.png"),
            },
            {
                name: "Docker",
                image: require("./img/docker.png"),
            },
            {
                name: "Git",
                image: require("./img/git.png"),
            },
            {
                name: "Gitlab",
                image: require("./img/gitlab.png"),
            },
            {
                name: "MySQL",
                image: require("./img/mysql.png"),
            },
            {
                name: "Express",
            },
            {
                name: "Spring",
            },
            {
                name: "Kubernetes",
            },
            {
                name: "Jest",
            },
            {
                name: "JUnit",
            },
            {
                name: "Mockito",
            },
            {
                name: "Flexbox"
            },
        ]
    },
    {
        title: "Familiar",
        list: [
            {
                name: "CSS Grids",
            },
            {
                name: "MongoDB",
            },
            {
                name: "Mongoose",
            },
            {
                name: "Redis",
            },
            {
                name: "React Hooks",
                image: require("./img/react-hooks.png"),
            },
            {
                name: "Bootstrap",
            },
            {
                name: "jQuery",
                image: require("./img/jQuery.png"),
            },
            {
                name: "Webpack",
                image: require("./img/webpack.png"),
            },
            {
                name: "Gitlab CI",
                image: require("./img/gitlab.png"),
            },
            {
                name: "Python",
                image: require("./img/python.png"),
            },
            {
                name: "Helm",
            }
        ]
    },
    {
        title: "Learning",
        list: [
            {
                name: "GraphQL",
                image: require("./img/graphql.png"),
            },
            {
                name: "Apollo",
                image: require("./img/apollo.png"),
            },
            {
                name: "Typescript"
            },
        ]
    },
]

const Technologies = () => {
    const technologySections = [];

    technologies.forEach(section => {
        technologySections.push((
            <div className="technologies__section">
                <div className="technologies__title-box">
                    <h3 className="technologies__title">{section.title}</h3>
                </div>
                <div className="technologies__list">
                    {section.list.map(technology => (
                        <Technology {...technology} key={technology.name} />
                    ))}
                </div>
            </div>
        ));
    });

    return (
        <div className="technologies">
            { technologySections }
        </div>
    );
};

export default Technologies;