import React from 'react';
import "./Technology.scss";

const Technology = ({name, image}) => (
    <div className="technology">
        <span className="technology__name">{name}</span>
    </div>
);

export default Technology;