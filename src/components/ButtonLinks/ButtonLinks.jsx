import React from 'react';
import "./ButtonLinks.scss";

const ButtonLinks = ({buttons}) => (
    <div className={`button-links ${buttons.length === 1 ? 'button-links--single-button' : ''}`}>
        {buttons.map(button => <a href={button.link} target="_blank" className="button-links__button">{button.name}</a>)}
    </div>
);

export default ButtonLinks;