import React from 'react';
import "./Section.scss";

const Section = ({ children, sectionName }) => (
  <div id={ sectionName } className={`section ${sectionName ? `section__${sectionName.toLowerCase()}` : ''}`}>
      { children }
  </div>
);

export default Section;